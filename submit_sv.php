<?php 
session_start(); 
$pdo = new PDO('mysql:host=localhost;port=3306;dbname=quanlysinhvien','root', ''); 
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo '<pre>';
    var_dump($_SESSION);
    echo '</pre>';
    if ($_SERVER['REQUEST_METHOD']=== 'POST') {
        $date=date_create($_SESSION['date']);
        $date =date_format($date,"Y/d/m");
        $gender;
        if ($_SESSION['gender']=='Nam') {
            $gender=0;
        } else {
            $gender=1;
        }
        $faculty;
        if ($_SESSION['faculty']=='Khoa học máy tính') {
            $faculty='MAT';
        } else {
            $faculty='KDL';
        }
        $statement = $pdo-> prepare("INSERT INTO  student (username, gender, faculty, birthday, address,avatar)
        VALUE (:username,:gender,:faculty,:birthday,:address,:avatar)");
        $statement ->bindValue(':username',$_SESSION['username']);
        $statement ->bindValue(':gender',$gender);
        $statement ->bindValue(':faculty',$faculty);
        $statement ->bindValue(':birthday',$date);
        $statement ->bindValue(':address',$_SESSION['address']);
        $statement ->bindValue(':avatar', $_SESSION['image']);
        $statement ->execute();
        header('Location: complete_regist.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<style>
    * {
        text-align: center;
    }

    fieldset {
        width: 470px;
        margin: 12%;
        margin-left: 32%;
        border: 1px solid #00AA00;
    }

    .username {
        background-color: #5b9bd5;
        color: azure;
        padding: 10px 20px;
        margin-right: 25px;
        width: 100px;
        text-align: center;
        border: 1px solid #00AA00
    }

    .text {
        border: 1px solid #00AA00;
        padding: 10px;
        width: 278px;
    }

    .apartment {
        margin-top: 20px;

    }

    .faculty {
        width: 300px;
        border: 1px solid #00AA00;
    }

    .button {
        width: 100px;
        margin-top: 30px;
        height: 30px;
        background-color: #008000;
        border: 1px solid #00AA00;
        border-radius: 5px;
        color: black;
    }
    
    .button:hover {
    opacity: 0.5;
}

    .style {
        display: flex;
    }

    .text {
        border: 1px solid #00AA00;
    }

    .notnull {
        color: red;
    }
</style>
<body>
    <form method="post">
        <fieldset class="day05-forn">
            <div class="forn">
                <div class="apartment">
                    <div class="style">
                        <div class="username">
                            <label>
                                Họ và tên <sup class="notnull">*</sup>
                            </label>
                        </div>
                        <td><p>
                        <?php echo $_SESSION["username"]; ?>
                        </p></td>
                    </div>
                </div>
                <div class="apartment">
                    <div class="style">
                        <div class="username">
                            <label>
                                Giới tính<sup class="notnull">*</sup>
                            </label>
                        </div>
                        <td><p>
                        <?php echo $_SESSION["gender"]; ?>
                        </p></td>
                    </div>
                </div>
                <div>
                </div>
            </div>
            </div>
            <div class="apartment">
                <div class="style">
                    <div class="username">
                        <label>
                            Phân khoa <sup class="notnull">*</sup>
                        </label>
                    </div>
                    <td><p>
                        <?php echo $_SESSION["faculty"]; ?>
                        </p></td>
                </div>
                <div class="apartment">
                    <div class="style">
                        <div class="username">
                            <label>
                                Ngày sinh <sup class="notnull">*</sup>
                            </label>
                        </div>
                        <td><p>
                        <?php echo $_SESSION["date"]; ?>
                        </p></td>
                    </div>
                </div>
                <div>
                    <div class="apartment">
                        <div class="style">
                            <div class="username">
                                <label>
                                    Địa chỉ
                                </label>
                            </div>
                            <td><p>
                        <?php echo $_SESSION["address"]; ?>
                        </p></td>
                        </div>
                    </div>
                </div>
                </div>
                <div>
                    <div class="apartment">
                        <div class="style">
                            <div class="username">
                                <label>
                                    Hình ảnh
                                </label>
                            </div>
                            <td>
                
                   <?php echo "<br> <img src = '" . $_SESSION["image"] ."  ' width = '100' height = '50'>";
                   echo "<br>"
                   ?>
                   </td>
                    
                        </div>
                    </div>
                </div>
                <input type="submit" name = "button" class="button" value="Xác nhận" />
            </div>

    </form>
    </fieldset>
</body>

</html>
