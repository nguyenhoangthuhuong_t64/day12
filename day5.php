<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<style>
    * {
        text-align: center;
    }

    fieldset {
        width: 470px;
        margin: 12%;
        margin-left: 32%;
        border: 1px solid #00AA00;
    }

    .username {
        background-color: #48c76e;
        color: azure;
        padding: 10px 20px;
        margin-right: 25px;
        width: 100px;
        text-align: center;
        border: 1px solid #00AA00
    }

    .text {
        border: 1px solid #00AA00;
        padding: 10px;
        width: 278px;
    }

    .apartment {
        margin-top: 20px;

    }

    .faculty {
        width: 300px;
        border: 1px solid #00AA00;
    }

    .button {
        width: 100px;
        margin-top: 30px;
        height: 30px;
        background-color: #008000;
        border: 1px solid #00AA00;
        border-radius: 5px;
        color: black;
    }
    
    .button:hover {
    opacity: 0.8;
}

    .style {
        display: flex;
    }

    .text {
        border: 1px solid #00AA00;
	text-align: left;
    }

    .notnull {
        color: red;
    }
</style>

<body>
    <?php
    session_start();
    $error = array();
    $data = array();
    $valid = true;
    $checkupload = true;
    $_SESSION = $_POST;
    if (!empty($_POST['button'])) {
        
        $_SESSION['username'] = isset($_POST['username']) ? $_POST['username'] : '';
        $_SESSION['gender'] = isset($_POST['gender']) ? $_POST['gender'] : '';
        $_SESSION['faculty'] = isset($_POST['faculty']) ? $_POST['faculty'] : '--faculty--';
        $_SESSION['date'] = isset($_POST['date']) ? $_POST['date'] : '';
	    $_SESSION['address']=isset($_POST['address']) ? $_POST['address'] : '';
        $_SESSION['image'] = isset($_POST['button']) &&($_POST['button']);
        
        if (empty($_SESSION['username'])) {
            $error['username'] = 'Hãy nhập tên';
        }

        if (empty($_SESSION['gender'])) {
            $error['gender'] = 'Hãy chọn giới tính';
        }

        if (empty($_SESSION['faculty']) || $_SESSION['faculty'] == '--faculty--') {
            $error['faculty'] = 'Hãy chọn phân khoa ';
        }

        if (empty($_SESSION['date'])) {
            $error['date'] = 'Hãy nhập ngày sinh';
        }

        if(!isset($_FILES["imageFile"])){
            array_push($error,"File upload này đã tồn tại.");
            $valid = false;
        }

        if ($_FILES["imageFile"]['error'] != 0)
        {
            $valid = false;
        }
        
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["imageFile"]["tmp_name"]);
            if($check !== false)
            {
                $valid = true;
            }
            else
            {
                $valid = false;
            }
        }
        $target_dir = "uploads/";
        $target_file   = $target_dir . basename($_FILES["imageFile"]["name"]);
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        $checktypes    = array('jpg', 'png', 'jpeg', 'gif');
        $maxfilesize   = 800000; 
        
        if (file_exists($target_file))
        {
            array_push($error,"File upload không tồn tại.");
            
            $valid = false;
        } elseif (!in_array($imageFileType,$checktypes)) {
            array_push($error,"File upload phải là định dạng JPG, PNG, JPEG, GIF");
            $valid = false;
        }
        
        if ($_FILES["imageFile"]["size"] > $maxfilesize)
        {
            array_push($error, "Không được upload ảnh lớn hơn $maxfilesize (bytes).");
            $valid = false;
        }
        
        if (!$error) {
            echo 'Dữ liệu có thể lưu trữ';
        } else {
            echo 'Dữ liệu bị lỗi, không thể lưu trữ';
        }
        if(empty($error)) {
            header("Location: submit_sv.php");
            
     }
    }

    ?>
    <form method="post" enctype="multipart/form-data" action="">
        <fieldset class="day05-forn">
            <div class="forn">
                <?php echo isset($error['username']) ? $error['username'] : ''; ?> <br />
                <?php echo isset($error['gender']) ? $error['gender'] : ''; ?> <br />
                <?php echo isset($error['faculty']) ? $error['faculty'] : ''; ?> <br />
                <?php echo isset($error['date']) ? $error['date'] : ''; ?> <br />
                <?php echo isset($error['address']) ? $error['address'] : ''; ?> <br />
                <div class="apartment">
                    <div class="style">
                        <div class="username">
                            <label>
                                Họ và tên <sup class="notnull">*</sup>
                            </label>
                        </div>
                        <input type="text" class="text" id="username" name="username">
                    </div>
                </div>
                <div class="apartment">
                    <div class="style">
                        <div class="username">
                            <label>
                                Giới tính<sup class="notnull">*</sup>
                            </label>
                        </div>
                        <?php
                        $gender = array("Nam", "Nữ");
                        $keys = array_keys($gender);
                        for ($i = 0; $i <= count($gender) - 1; $i++) { ?>
                            <input type="radio" , name="gender" id = "gender" checked="<?php echo "checked"; 
                            ?>" value="<?php echo $gender[$i]; ?>"> <?php echo $gender[$i]; ?>

                        <?php
                        } ?>
                    </div>
                </div>
                <div>
                </div>
            </div>
            </div>
            <div class="apartment">
                <div class="style">
                    <div class="username">
                        <label>
                            Phân khoa <sup class="notnull">*</sup>
                        </label>
                    </div>
                    <select class="faculty" name="faculty" id="faculty">
                        <?php $faculty = array('' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu'); 
                             foreach ($faculty as $key => $value) {  
                                echo " <option "; 
                                echo isset($_POST['Faculty']) && $_POST['Faculty'] == $key ? " selected " : ""; 
                                echo " value='" . $key . "'>" . $value . "</option> "; 
                            }
                         ?>
                    </select>
                </div>
                <div class="apartment">
                    <div class="style">
                        <div class="username">
                            <label>
                                Ngày sinh <sup class="notnull">*</sup>
                            </label>
                        </div>
                        <input type="date" name="date" id="date" class="date">
                    </div>
                </div>
                <div>
                    <div class="apartment">
                        <div class="style">
                            <div class="username">
                                <label>
                                    Địa chỉ
                                </label>
                            </div>
                            <input type="text" class="text" id="address" name="address">
                            <?php
                            if (!empty($_SESSION["address"])){
                                echo  $_SESSION["address"]; 
                            }
                            ?>                
                        </div>
                    </div>
                </div>
                </div>
                <div>
                    <div class="apartment">
                        <div class="style">
                            <div class="username">
                                <label>
                                    Hình ảnh
                                </label>
                            </div>
                            <input type="file" class="image" id="image" name="imageFile">
                            <?php
                            if (isset($_POST['button']) &&($_POST['button']))
                            {
                                if ($valid){
                                    if (!file_exists($target_dir)){
                                        mkdir($target_dir, 0777, true);
                                    }
                                    $target_file = $target_dir . pathinfo($_FILES['imageFile']['name'], PATHINFO_FILENAME)."_".date('YmdHis').".".pathinfo($target_file,PATHINFO_EXTENSION);
                                    if (move_uploaded_file($_FILES["imageFile"]["tmp_name"], $target_file)) {
                                        $_SESSION["image"] =$target_file;
                                    } else {
                                        $_SESSION["image"] = "Sorry, there was an error uploading your file.";
                                    }
                                }
                                
     
                            }

                        echo ""
                            ?>
    
                        </div>
                    </div>
                </div>
                <input type="submit" name = "button" class="button" value="Đăng ký" />
            </div>

    </form>
    </fieldset>
</body>

</html>
